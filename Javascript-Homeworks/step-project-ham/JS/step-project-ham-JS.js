function choiceService() {
    const buttons = document.querySelectorAll('.tabs-title');
    buttons.forEach(i => i.addEventListener('click', (event) => {
        document.querySelector('.active').classList.remove('active');
        document.querySelectorAll('.description').forEach(el => el.classList.remove('active-two'));
        event.target.classList.add('active');
        const activeClass = event.target.textContent.replace(/\s/g, '');
        const activeTextTwo = activeClass.toLowerCase();
        document.querySelector(`.${activeTextTwo}`).classList.add('active-two');

    }));
}

choiceService()


const arrPortfolio = [

    {
        type: 'img',
        name: 'Graphic Design',
        src: 'Assets/graphic-design/graphic-design4.jpg',
        data: 'graphic',
    },

    {
        type: 'img',
        name: 'Graphic Design',
        src: 'Assets/graphic-design/graphic-design5.jpg',
        data: 'graphic',
    },
    {
        type: 'img',
        name: 'Graphic Design',
        src: 'Assets/graphic-design/graphic-design6.jpg',
        data: 'graphic',
    },

    {
        type: 'img',
        name: 'Graphic Design',
        src: 'Assets/graphic-design/graphic-design7.jpg',
        data: 'graphic',
    },

    {
        type: 'img',
        name: 'Graphic Design',
        src: 'Assets/graphic-design/graphic-design8.jpg',
        data: 'graphic',
    },


    {
        type: 'img',
        name: 'Web Design',
        src: 'Assets/web-design/web-design4.jpg',
        data: 'web',
    },
    {
        type: 'img',
        name: 'Web Design',
        src: 'Assets/web-design/web-design5.jpg',
        data: 'web',
    },
    {
        type: 'img',
        name: 'Web Design',
        src: 'Assets/web-design/web-design6.jpg',
        data: 'web',
    },

    {
        type: 'img',
        name: 'Landing Pages',
        src: 'Assets/landing-page/landing-page4.jpg',
        data: 'landing',
    },
    {
        type: 'img',
        name: 'Landing Pages',
        src: 'Assets/landing-page/landing-page5.jpg',
        data: 'landing',
    },
    {
        type: 'img',
        name: 'Landing Pages',
        src: 'Assets/landing-page/landing-page6.jpg',
        data: 'landing',
    },

    {
        type: 'img',
        name: 'Wordpress',
        src: 'Assets/wordpress/wordpress4.jpg',
        data: 'wordpress',
    },
    {
        type: 'img',
        name: 'Wordpress',
        src: 'Assets/wordpress/wordpress5.jpg',
        data: 'wordpress',
    },
    {
        type: 'img',
        name: 'Wordpress',
        src: 'Assets/wordpress/wordpress6.jpg',
        data: 'wordpress',
    },
    {
        type: 'img',
        name: 'Wordpress',
        src: 'Assets/wordpress/wordpress7.jpg',
        data: 'wordpress',
    },
]

let portfolioElements = [];


const arrImg = document.querySelector('.amazing-content');


const buttonsWork = document.querySelectorAll('.amazing-tabs-title');
buttonsWork.forEach(a => a.addEventListener('click', (event) => {
    document.querySelector('.active-amazing').classList.remove('active-amazing');
    event.target.classList.add('active-amazing');
    const photoFilter = document.querySelectorAll('.amazing-portfolio')

    photoFilter.forEach(i => {
        if (event.target.dataset.tab === i.dataset.tab) {
            i.classList.remove('hide')
        } else {
            i.classList.add('hide')
        }
    });
    if (event.target.dataset.tab === 'all') {
        photoFilter.forEach(variable)
    }

    function variable(item, index) {
        const buttonUser = document.querySelector('.button-style-load');
        if (!buttonUser) {
            item.classList.remove('hide')
        } else if (index < 12) {
            item.classList.remove('hide')
        }
    }

}));

function loadMode() {
    const buttonUser = document.querySelector('.button-style-load');
    buttonUser.addEventListener('click', (event) => {
        arrPortfolio.forEach(addElements)

        function addElements(item, index) {
            if (index < 12) {
                arrImg.insertAdjacentHTML('beforeend',
                    `<div class="amazing-portfolio" name="${item.name}" data-tab="${item.data}"><img class="img-size" src="${item.src}"  alt=""><div class="amazing-hover">
                    <a href="#">
                        <div class="amazing-hover-sings-block">
                            <div class="hover-sings-one">
                                <svg class="hover-sings-one-share" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path class="hover-sings-one-share"  d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                                </svg>
                            </div>
                        <div class="hover-sings-two">
                        <div class="hover-sings-square">
                        </div>

                    </div>
                    </div>
                    </a>
                    <p class="amazing-hover-text">creative design</p>
                     <p class="amazing-hover-text-two">${item.name}</p>
                </div>
                </div>`
                )
            }
        }

        buttonUser.remove()

    });
}

loadMode()

function choiceSlider() {
    const buttonsSlider = document.querySelectorAll('.button-photo');
    buttonsSlider.forEach(i => i.addEventListener('click', (event) => {
        document.querySelector('.button-photo-active').classList.remove('button-photo-active');
        document.querySelectorAll('.slider-block-item').forEach(el => el.classList.remove('slider-block-item-active'));
        event.target.classList.add('button-photo-active');
        const activeClass = event.target.dataset.reg;
        document.querySelector(`.${activeClass}`).classList.add('slider-block-item-active');

    }));
}

choiceSlider()

const caruselSlider = document.querySelector('.carusel-slider');
const caruselImg = document.querySelectorAll('.carusel-slider img');
const buttonPhoto = document.querySelectorAll('.button-photo');
const buttonPrev = document.querySelector('#prevBtn');
const buttonNext = document.querySelector('#nextBtn');

let counter = 4;
let size = 102;
let index = 8;
let num = 12;

caruselSlider.style.transform = 'translateX(' + (-size * counter) + 'px)';
const allSlides = document.querySelectorAll('.slider-block-item');

buttonNext.addEventListener('click', () => {
    if (counter >= caruselImg.length - 1) return;
    caruselSlider.style.transition = 'transform 0.4s ease-in-out';
    allSlides[index].classList.remove('slider-block-item-active')

    buttonPhoto[num].classList.remove('button-photo-active')
    counter++;
    index++;
    num++

    if (index === allSlides.length) { index = 0 }
    if (num === 16) { num = 6 }
    caruselSlider.style.transform = 'translateX(' + (-size * counter) + 'px)';
    allSlides[index].classList.add('slider-block-item-active');
    buttonPhoto[num].classList.add('button-photo-active');

});


buttonPrev.addEventListener('click', () => {
    if (counter >= caruselImg.length - 1) return;
    caruselSlider.style.transition = 'transform 0.4s ease-in-out';
    allSlides[index].classList.remove('slider-block-item-active');
    buttonPhoto[num].classList.remove('button-photo-active');
    if (index === 0) {
        index = allSlides.length
    }
    if (num === 0) {
        num = 10
    }
    counter--;
    index--;
    num--
    allSlides[index].classList.add('slider-block-item-active');
    buttonPhoto[num].classList.add('button-photo-active');

    caruselSlider.style.transform = 'translateX(' + (-size * counter) + 'px)';

});

caruselSlider.addEventListener("transitionend", () => {
    if (-size * counter <= -812) {
        counter = -2;
        caruselSlider.style.transition = 'none';
        caruselSlider.style.transform = 'translateX(' + (-size * counter) + 'px)';
    }
    if (-size * counter >= 812) {
        counter = 2;
        caruselSlider.style.transition = 'none';
        caruselSlider.style.transform = 'translateX(' + (-size * counter) + 'px)';
    }
});
