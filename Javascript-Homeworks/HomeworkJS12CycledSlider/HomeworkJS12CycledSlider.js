const images = document.querySelectorAll(".image-to-show");
const stopButton = document.querySelector(".button-stop");
const playButton = document.querySelector(".button-play");
let counter = 0;

function clickOnButton() {
    document.addEventListener("click", event => {
        if (event.target.classList === stopButton.classList) {
            clearTimeout(showImagesWithDelay);
        } else if (event.target.classList === playButton.classList) {
            clearTimeout(showImagesWithDelay);
            showImages();
        }
    });

    let showImagesWithDelay;

    function showImages() {
        showImagesWithDelay = setTimeout(function showImage()  {
            counter++;
            if (counter !== 0) {
                images[counter - 1].hidden = true;
            }

            if (counter / images.length === 1) {
                counter = 0;
            }

            images[counter].hidden = false;
            setTimeout(showImage,3000);
        }, 3000);
    }

    showImages()
}

clickOnButton();
