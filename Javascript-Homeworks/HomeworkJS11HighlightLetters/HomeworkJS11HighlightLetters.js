function blueButton() {
    const buttons = document.querySelectorAll('.btn');
    document.addEventListener('keydown', function(event) {

        buttons.forEach(item => {
            if (event.key.toLowerCase() === item.textContent.toLowerCase()) {
                item.classList.add('blue-button');
            } else {
                item.classList.remove('blue-button');
            }
        })
    })
}

blueButton()
