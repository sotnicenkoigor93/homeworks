function priceValidator() {
    const priceWrapper = document.getElementById("price-input-wrapper")

    const priceSpan = document.querySelector(".price-span");

    const priceSpanClearButton = document.querySelector(".price-clear-button");

    const priceInput = document.querySelector(".price-input");

    const priceErrorSpan = document.querySelector(".price-error-span");

    priceInput.addEventListener("focus", event =>{
        priceInput.classList.add("right")

    })

    priceInput.addEventListener("blur", event => {
        if (priceInput.value === '' || priceInput.value <= 0){
            priceInput.className = "price-input wrong";
            priceErrorSpan.classList.add("active");
            priceErrorSpan.textContent = "Please enter the correct price";
            priceSpan.className = "price-span";
            priceSpanClearButton.className = "price-clear-button";
        }else if (priceInput.value > "0" ) {
            priceInput.classList.add("right");
            priceSpan.classList.add("active");
            priceSpan.textContent = `The price is: $${priceInput.value}`;
            priceErrorSpan.className = "price-error-span";
            priceSpanClearButton.classList.add("active");

            priceSpanClearButton.addEventListener("click", e =>{
                priceInput.value = "";
                priceSpan.className ="price-span";
                priceSpanClearButton.className = "price-clear-button";
            });
        }
    })
    return priceWrapper.append(priceSpan,priceSpanClearButton,priceInput,priceErrorSpan);
}

priceValidator();



