function createListOfItems(array , parentElement = document.body) {
    const ul = document.createElement("ul");
    ul.innerHTML = array.map((item) => `<li>${item}</li>`).join("");
    parentElement.append(ul);
}
createListOfItems(["hello", "world", "Kyiv", "Kharkiv", "Odessa", "Lviv" ]);